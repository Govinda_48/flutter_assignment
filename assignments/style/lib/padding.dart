import 'package:flutter/material.dart';

class File2 extends StatelessWidget {
  const File2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Container App',
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.amber,
      ),
      body: Center(
        child: Container(
          // padding: const EdgeInsets.all(20),
          padding:
              const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
          width: 100,
          height: 100,
          color: Colors.blue,
        ),
      ),
    );
  }
}
