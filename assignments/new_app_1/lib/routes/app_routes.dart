import 'package:flutter/material.dart';
import '../presentation/screen_2_english_screen/screen_2_english_screen.dart';
import '../presentation/login_screen/login_screen.dart';
import '../presentation/screen_1_english_screen/screen_1_english_screen.dart';
import '../presentation/app_navigation_screen/app_navigation_screen.dart';

class AppRoutes {
  static const String screen2EnglishScreen = '/screen_2_english_screen';

  static const String loginScreen = '/login_screen';

  static const String screen1EnglishScreen = '/screen_1_english_screen';

  static const String appNavigationScreen = '/app_navigation_screen';

  static const String initialRoute = '/initialRoute';

  static Map<String, WidgetBuilder> get routes => {
        screen2EnglishScreen: Screen2EnglishScreen.builder,
        loginScreen: LoginScreen.builder,
        screen1EnglishScreen: Screen1EnglishScreen.builder,
        appNavigationScreen: AppNavigationScreen.builder,
        initialRoute: Screen2EnglishScreen.builder
      };
}
