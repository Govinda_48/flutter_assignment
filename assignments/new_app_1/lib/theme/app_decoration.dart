import 'package:flutter/material.dart';
import 'package:sagar_s_application2/core/app_export.dart';

class AppDecoration {
  // Fill decorations
  static BoxDecoration get fillGray => BoxDecoration(
        color: appTheme.gray40000.withOpacity(0.25),
      );
  static BoxDecoration get fillOnPrimaryContainer => BoxDecoration(
        color: theme.colorScheme.onPrimaryContainer,
      );

  // Gradient decorations
  static BoxDecoration get gradientDeepPurpleAToCyan => BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(-0.13, -0.06),
          end: Alignment(1.13, 1.08),
          colors: [
            appTheme.deepPurpleA100,
            appTheme.cyan20001,
          ],
        ),
      );
  static BoxDecoration get gradientDeepPurpleToCyan => BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(0.08, 0.94),
          end: Alignment(0.92, 0.03),
          colors: [
            appTheme.deepPurple30002,
            appTheme.cyan20004,
          ],
        ),
      );
  static BoxDecoration get gradientDeepPurpleToIndigoA => BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(1.08, 1.08),
          end: Alignment(-0.05, -0.19),
          colors: [
            appTheme.deepPurple30001,
            appTheme.indigoA10001,
          ],
        ),
      );
  static BoxDecoration get gradientDeepPurpleToIndigoA10001 => BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(1.08, 1.08),
          end: Alignment(-0.05, -0.19),
          colors: [
            appTheme.deepPurple30003,
            appTheme.indigoA10001,
          ],
        ),
      );
  static BoxDecoration get gradientGreenAToBlue => BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(0.03, 0),
          end: Alignment(1.28, 1.5),
          colors: [
            appTheme.greenA20002,
            appTheme.blue20001,
          ],
        ),
      );
  static BoxDecoration get gradientSecondaryContainerToGray => BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(0.5, 0),
          end: Alignment(0.5, 1),
          colors: [
            theme.colorScheme.secondaryContainer,
            appTheme.gray40000,
          ],
        ),
      );
  static BoxDecoration get gradientTealAToIndigoA => BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(-0.13, -0.06),
          end: Alignment(1.13, 1.08),
          colors: [
            appTheme.tealA20001,
            appTheme.indigoA10002,
          ],
        ),
      );
}

class BorderRadiusStyle {
  // Circle borders
  static BorderRadius get circleBorder31 => BorderRadius.circular(
        31.h,
      );

  // Custom borders
  static BorderRadius get customBorderBL40 => BorderRadius.vertical(
        bottom: Radius.circular(40.h),
      );

  // Rounded borders
  static BorderRadius get roundedBorder10 => BorderRadius.circular(
        10.h,
      );
  static BorderRadius get roundedBorder15 => BorderRadius.circular(
        15.h,
      );
  static BorderRadius get roundedBorder21 => BorderRadius.circular(
        21.h,
      );
  static BorderRadius get roundedBorder25 => BorderRadius.circular(
        25.h,
      );
  static BorderRadius get roundedBorder40 => BorderRadius.circular(
        40.h,
      );
}

// Comment/Uncomment the below code based on your Flutter SDK version.

// For Flutter SDK Version 3.7.2 or greater.

double get strokeAlignInside => BorderSide.strokeAlignInside;

double get strokeAlignCenter => BorderSide.strokeAlignCenter;

double get strokeAlignOutside => BorderSide.strokeAlignOutside;

// For Flutter SDK Version 3.7.1 or less.

// StrokeAlign get strokeAlignInside => StrokeAlign.inside;
//
// StrokeAlign get strokeAlignCenter => StrokeAlign.center;
//
// StrokeAlign get strokeAlignOutside => StrokeAlign.outside;
