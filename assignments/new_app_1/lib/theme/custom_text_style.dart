import 'package:flutter/material.dart';
import '../core/app_export.dart';

/// A collection of pre-defined text styles for customizing text appearance,
/// categorized by different font families and weights.
/// Additionally, this class includes extensions on [TextStyle] to easily apply specific font families to text.

class CustomTextStyles {
  // Body text style
  static get bodySmall9 => theme.textTheme.bodySmall!.copyWith(
        fontSize: 9.fSize,
      );
  // Headline text style
  static get headlineSmallBlack900 => theme.textTheme.headlineSmall!.copyWith(
        color: appTheme.black900,
        fontSize: 25.fSize,
        fontWeight: FontWeight.w800,
      );
  static get headlineSmallff000000 => theme.textTheme.headlineSmall!.copyWith(
        color: Color(0XFF000000),
        fontSize: 25.fSize,
        fontWeight: FontWeight.w800,
      );
  static get headlineSmallff000000Regular =>
      theme.textTheme.headlineSmall!.copyWith(
        color: Color(0XFF000000),
        fontSize: 25.fSize,
        fontWeight: FontWeight.w400,
      );
  // Label text style
  static get labelLargeOnPrimaryContainer =>
      theme.textTheme.labelLarge!.copyWith(
        color: theme.colorScheme.onPrimaryContainer,
        fontSize: 13.fSize,
        fontWeight: FontWeight.w700,
      );
  static get labelLargeOnPrimaryContainerBold =>
      theme.textTheme.labelLarge!.copyWith(
        color: theme.colorScheme.onPrimaryContainer,
        fontSize: 13.fSize,
        fontWeight: FontWeight.w700,
      );
  static get labelLargeOnPrimaryContainerBold_1 =>
      theme.textTheme.labelLarge!.copyWith(
        color: theme.colorScheme.onPrimaryContainer,
        fontWeight: FontWeight.w700,
      );
  static get labelMediumIndigoA100 => theme.textTheme.labelMedium!.copyWith(
        color: appTheme.indigoA100,
      );
  static get labelMedium_1 => theme.textTheme.labelMedium!;
  // Nova text style
  static get novaOvalff649bec => TextStyle(
        color: Color(0XFF649BEC),
        fontSize: 77.fSize,
        fontWeight: FontWeight.w400,
      ).novaOval;
  static get novaOvalff84efc2 => TextStyle(
        color: Color(0XFF84EFC2),
        fontSize: 77.fSize,
        fontWeight: FontWeight.w400,
      ).novaOval;
  // Nunito text style
  static get nunitoBlack900 => TextStyle(
        color: appTheme.black900,
        fontSize: 77.fSize,
        fontWeight: FontWeight.w700,
      ).nunito;
  // Title text style
  static get titleLarge23 => theme.textTheme.titleLarge!.copyWith(
        fontSize: 23.fSize,
      );
  static get titleLargeNovaOvalff649bec =>
      theme.textTheme.titleLarge!.novaOval.copyWith(
        color: Color(0XFF649BEC),
        fontWeight: FontWeight.w400,
      );
  static get titleLargeNovaOvalff84efc2 =>
      theme.textTheme.titleLarge!.novaOval.copyWith(
        color: Color(0XFF84EFC2),
        fontWeight: FontWeight.w400,
      );
  static get titleMediumBlack900 => theme.textTheme.titleMedium!.copyWith(
        color: appTheme.black900,
      );
  static get titleMediumBlue20001 => theme.textTheme.titleMedium!.copyWith(
        color: appTheme.blue20001,
        fontSize: 18.fSize,
      );
  static get titleMediumOnPrimaryContainer =>
      theme.textTheme.titleMedium!.copyWith(
        color: theme.colorScheme.onPrimaryContainer,
        fontSize: 18.fSize,
      );
  static get titleMediumOnPrimaryContainer17 =>
      theme.textTheme.titleMedium!.copyWith(
        color: theme.colorScheme.onPrimaryContainer,
        fontSize: 17.fSize,
      );
  static get titleMediumOnPrimaryContainer17_1 =>
      theme.textTheme.titleMedium!.copyWith(
        color: theme.colorScheme.onPrimaryContainer,
        fontSize: 17.fSize,
      );
  static get titleMediumOnPrimaryContainer18 =>
      theme.textTheme.titleMedium!.copyWith(
        color: theme.colorScheme.onPrimaryContainer,
        fontSize: 18.fSize,
      );
}

extension on TextStyle {
  TextStyle get nunito {
    return copyWith(
      fontFamily: 'Nunito',
    );
  }

  TextStyle get novaOval {
    return copyWith(
      fontFamily: 'Nova Oval',
    );
  }
}
