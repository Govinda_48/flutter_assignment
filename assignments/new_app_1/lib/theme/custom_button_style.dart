import 'package:flutter/material.dart';
import '../core/app_export.dart';

/// A class that offers pre-defined button styles for customizing button appearance.
class CustomButtonStyles {
  // Gradient button style
  static BoxDecoration get gradientGreenAToBlueDecoration => BoxDecoration(
        borderRadius: BorderRadius.circular(13.h),
        boxShadow: [
          BoxShadow(
            color: appTheme.black900.withOpacity(0.1),
            spreadRadius: 2.h,
            blurRadius: 2.h,
            offset: Offset(
              0,
              4,
            ),
          ),
        ],
        gradient: LinearGradient(
          begin: Alignment(0.03, 0),
          end: Alignment(1.28, 1),
          colors: [
            appTheme.greenA20002,
            appTheme.blue20001,
          ],
        ),
      );
  static BoxDecoration get gradientGreenAToCyanDecoration => BoxDecoration(
        borderRadius: BorderRadius.circular(23.h),
        boxShadow: [
          BoxShadow(
            color: appTheme.gray7003f,
            spreadRadius: 2.h,
            blurRadius: 2.h,
            offset: Offset(
              3,
              4,
            ),
          ),
        ],
        gradient: LinearGradient(
          begin: Alignment(-0.07, 0),
          end: Alignment(1.2, 0),
          colors: [
            appTheme.greenA200,
            appTheme.cyan200,
          ],
        ),
      );
  static BoxDecoration get gradientGreenAToGreenADecoration => BoxDecoration(
        borderRadius: BorderRadius.circular(13.h),
        boxShadow: [
          BoxShadow(
            color: appTheme.black900.withOpacity(0.1),
            spreadRadius: 2.h,
            blurRadius: 2.h,
            offset: Offset(
              0,
              4,
            ),
          ),
        ],
        gradient: LinearGradient(
          begin: Alignment(0.03, 0),
          end: Alignment(1.28, 1),
          colors: [
            appTheme.greenA20001,
            appTheme.greenA100,
          ],
        ),
      );
  static BoxDecoration get gradientPrimaryToIndigoADecoration => BoxDecoration(
        borderRadius: BorderRadius.circular(24.h),
        gradient: LinearGradient(
          begin: Alignment(0.02, 1),
          end: Alignment(1.26, 0),
          colors: [
            theme.colorScheme.primary,
            appTheme.indigoA100,
          ],
        ),
      );

  // Outline button style
  static ButtonStyle get outlineTL11 => OutlinedButton.styleFrom(
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(11.h),
        ),
      );
  // text button style
  static ButtonStyle get none => ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(Colors.transparent),
        elevation: MaterialStateProperty.all<double>(0),
      );
}
