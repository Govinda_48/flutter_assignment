// ignore_for_file: must_be_immutable

part of 'screen_1_english_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///Screen1English widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class Screen1EnglishEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

/// Event that is dispatched when the Screen1English widget is first created.
class Screen1EnglishInitialEvent extends Screen1EnglishEvent {
  @override
  List<Object?> get props => [];
}
