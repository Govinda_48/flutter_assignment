// ignore_for_file: must_be_immutable

part of 'screen_1_english_bloc.dart';

/// Represents the state of Screen1English in the application.
class Screen1EnglishState extends Equatable {
  Screen1EnglishState({
    this.sliderIndex = 0,
    this.screen1EnglishModelObj,
  });

  Screen1EnglishModel? screen1EnglishModelObj;

  int sliderIndex;

  @override
  List<Object?> get props => [
        sliderIndex,
        screen1EnglishModelObj,
      ];

  Screen1EnglishState copyWith({
    int? sliderIndex,
    Screen1EnglishModel? screen1EnglishModelObj,
  }) {
    return Screen1EnglishState(
      sliderIndex: sliderIndex ?? this.sliderIndex,
      screen1EnglishModelObj:
          screen1EnglishModelObj ?? this.screen1EnglishModelObj,
    );
  }
}
