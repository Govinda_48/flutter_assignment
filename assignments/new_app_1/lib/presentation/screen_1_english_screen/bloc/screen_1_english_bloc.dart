import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/fortynine_item_model.dart';
import '../models/userprofile_item_model.dart';
import 'package:sagar_s_application2/presentation/screen_1_english_screen/models/screen_1_english_model.dart';
part 'screen_1_english_event.dart';
part 'screen_1_english_state.dart';

/// A bloc that manages the state of a Screen1English according to the event that is dispatched to it.
class Screen1EnglishBloc
    extends Bloc<Screen1EnglishEvent, Screen1EnglishState> {
  Screen1EnglishBloc(Screen1EnglishState initialState) : super(initialState) {
    on<Screen1EnglishInitialEvent>(_onInitialize);
  }

  List<FortynineItemModel> fillFortynineItemList() {
    return [
      FortynineItemModel(
          text: "Air Terjun Luweng Sampang, Gunungkidul",
          image: ImageConstant.imgAirTerjunLuwe),
      FortynineItemModel(
          text: "Pantai Ngrumput, Gunungkidul",
          image: ImageConstant.imgPantaiWatuKodok),
      FortynineItemModel(
          text: "Watu Kodok, Gunungkidul",
          image: ImageConstant.imgPantaiWatuKodok158x144)
    ];
  }

  List<UserprofileItemModel> fillUserprofileItemList() {
    return [
      UserprofileItemModel(
          userImage: ImageConstant.imgEllipse9,
          userName: "Michelle Sauniere",
          distance: "1.7km"),
      UserprofileItemModel(
          userImage: ImageConstant.imgEllipse12,
          userName: "Angela Lahm",
          distance: "6.7km"),
      UserprofileItemModel(
          userImage: ImageConstant.imgEllipse1182x82,
          userName: "Trevor McHall",
          distance: "2.7km"),
      UserprofileItemModel(
          userImage: ImageConstant.imgEllipse111,
          userName: "Phillips Eduardo",
          distance: "1.8km")
    ];
  }

  _onInitialize(
    Screen1EnglishInitialEvent event,
    Emitter<Screen1EnglishState> emit,
  ) async {
    emit(state.copyWith(sliderIndex: 0));
    emit(state.copyWith(
        screen1EnglishModelObj: state.screen1EnglishModelObj?.copyWith(
            fortynineItemList: fillFortynineItemList(),
            userprofileItemList: fillUserprofileItemList())));
  }
}
