import '../../../core/app_export.dart';

/// This class is used in the [userprofile_item_widget] screen.
class UserprofileItemModel {
  UserprofileItemModel({
    this.userImage,
    this.userName,
    this.distance,
    this.id,
  }) {
    userImage = userImage ?? ImageConstant.imgEllipse9;
    userName = userName ?? "Michelle Sauniere";
    distance = distance ?? "1.7km";
    id = id ?? "";
  }

  String? userImage;

  String? userName;

  String? distance;

  String? id;
}
