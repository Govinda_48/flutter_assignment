import '../../../core/app_export.dart';

/// This class is used in the [fortynine_item_widget] screen.
class FortynineItemModel {
  FortynineItemModel({
    this.text,
    this.image,
    this.id,
  }) {
    text = text ?? "Air Terjun Luweng Sampang, Gunungkidul";
    image = image ?? ImageConstant.imgAirTerjunLuwe;
    id = id ?? "";
  }

  String? text;

  String? image;

  String? id;
}
