// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';
import '../../../core/app_export.dart';
import 'fortynine_item_model.dart';
import 'userprofile_item_model.dart';

/// This class defines the variables used in the [screen_1_english_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class Screen1EnglishModel extends Equatable {
  Screen1EnglishModel({
    this.fortynineItemList = const [],
    this.userprofileItemList = const [],
  }) {}

  List<FortynineItemModel> fortynineItemList;

  List<UserprofileItemModel> userprofileItemList;

  Screen1EnglishModel copyWith({
    List<FortynineItemModel>? fortynineItemList,
    List<UserprofileItemModel>? userprofileItemList,
  }) {
    return Screen1EnglishModel(
      fortynineItemList: fortynineItemList ?? this.fortynineItemList,
      userprofileItemList: userprofileItemList ?? this.userprofileItemList,
    );
  }

  @override
  List<Object?> get props => [fortynineItemList, userprofileItemList];
}
