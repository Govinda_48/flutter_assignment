import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:outline_gradient_button/outline_gradient_button.dart';
import 'package:sagar_s_application2/core/app_export.dart';
import 'package:sagar_s_application2/widgets/app_bar/custom_app_bar.dart';
import 'package:sagar_s_application2/widgets/custom_elevated_button.dart';
import 'package:sagar_s_application2/widgets/custom_icon_button.dart';
import 'package:sagar_s_application2/widgets/custom_outlined_button.dart';
import 'bloc/screen_1_english_bloc.dart';
import 'models/fortynine_item_model.dart';
import 'models/screen_1_english_model.dart';
import 'models/userprofile_item_model.dart';
import 'widgets/fortynine_item_widget.dart';
import 'widgets/userprofile_item_widget.dart';

class Screen1EnglishScreen extends StatelessWidget {
  const Screen1EnglishScreen({Key? key}) : super(key: key);

  static Widget builder(BuildContext context) {
    return BlocProvider<Screen1EnglishBloc>(
        create: (context) => Screen1EnglishBloc(
            Screen1EnglishState(screen1EnglishModelObj: Screen1EnglishModel()))
          ..add(Screen1EnglishInitialEvent()),
        child: Screen1EnglishScreen());
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            body: SizedBox(
                width: double.maxFinite,
                child: Column(children: [
                  _buildNine(context),
                  Expanded(
                      child: SingleChildScrollView(
                          child: Padding(
                              padding: EdgeInsets.only(bottom: 5.v),
                              child: Column(children: [
                                SizedBox(
                                    height: 647.v,
                                    width: double.maxFinite,
                                    child: Stack(
                                        alignment: Alignment.bottomLeft,
                                        children: [
                                          _buildTen(context),
                                          _buildFortyNine(context)
                                        ])),
                                SizedBox(height: 34.v),
                                _buildThirtyFive(context),
                                SizedBox(height: 19.v),
                                _buildUserProfile(context)
                              ]))))
                ])),
            bottomNavigationBar: _buildBottomBar(context)));
  }

  /// Section Widget
  Widget _buildGunungkidulYogyakarta(BuildContext context) {
    return CustomElevatedButton(
        height: 36.v,
        width: 221.h,
        text: "msg_gunungkidul_yogyakarta".tr,
        leftIcon: Container(
            margin: EdgeInsets.only(right: 7.h),
            child: CustomImageView(
                imagePath: ImageConstant.imgLocation,
                height: 23.adaptSize,
                width: 23.adaptSize)),
        buttonStyle: CustomButtonStyles.none,
        decoration: CustomButtonStyles.gradientGreenAToGreenADecoration,
        buttonTextStyle: CustomTextStyles.labelLargeOnPrimaryContainer);
  }

  /// Section Widget
  Widget _buildNine(BuildContext context) {
    return Container(
        decoration:
            BoxDecoration(borderRadius: BorderRadiusStyle.customBorderBL40),
        child: Column(children: [
          CustomAppBar(
              leadingWidth: 75.h,
              leading: Container(
                  height: 45.adaptSize,
                  width: 45.adaptSize,
                  margin: EdgeInsets.only(left: 30.h, top: 54.v, bottom: 19.v),
                  child: Stack(alignment: Alignment.center, children: [
                    CustomIconButton(
                        height: 45.adaptSize,
                        width: 45.adaptSize,
                        alignment: Alignment.center,
                        child: CustomImageView()),
                    CustomImageView(
                        imagePath: ImageConstant.imgMegaphone,
                        height: 16.v,
                        width: 24.h,
                        alignment: Alignment.center,
                        margin: EdgeInsets.fromLTRB(10.h, 16.v, 11.h, 13.v))
                  ])),
              centerTitle: true,
              title: Padding(
                  padding: EdgeInsets.only(top: 64.v, bottom: 22.v),
                  child: RichText(
                      text: TextSpan(children: [
                        TextSpan(
                            text: "lbl_libur".tr,
                            style: CustomTextStyles.titleLargeNovaOvalff84efc2),
                        TextSpan(
                            text: "lbl_by".tr,
                            style: CustomTextStyles.titleLargeNovaOvalff649bec)
                      ]),
                      textAlign: TextAlign.left)),
              actions: [
                Container(
                    height: 45.adaptSize,
                    width: 45.adaptSize,
                    margin: EdgeInsets.fromLTRB(30.h, 54.v, 30.h, 19.v),
                    child: Stack(alignment: Alignment.topCenter, children: [
                      CustomIconButton(
                          height: 45.adaptSize,
                          width: 45.adaptSize,
                          alignment: Alignment.center,
                          child: CustomImageView()),
                      CustomImageView(
                          imagePath: ImageConstant.imgSearch,
                          height: 31.adaptSize,
                          width: 31.adaptSize,
                          alignment: Alignment.topCenter,
                          margin: EdgeInsets.all(7.h))
                    ]))
              ],
              styleType: Style.bgStyle),
          SizedBox(height: 8.v),
          Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                  padding: EdgeInsets.only(left: 30.h, right: 43.h),
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            height: 53.v,
                            width: 212.h,
                            margin: EdgeInsets.only(bottom: 7.v),
                            child: Stack(
                                alignment: Alignment.bottomCenter,
                                children: [
                                  Align(
                                      alignment: Alignment.topLeft,
                                      child: RichText(
                                          text: TextSpan(children: [
                                            TextSpan(
                                                text: "lbl_hey".tr,
                                                style: CustomTextStyles
                                                    .headlineSmallff000000Regular),
                                            TextSpan(
                                                text: "lbl_mchardyan".tr,
                                                style: CustomTextStyles
                                                    .headlineSmallff000000)
                                          ]),
                                          textAlign: TextAlign.left)),
                                  Align(
                                      alignment: Alignment.bottomCenter,
                                      child: Text("msg_where_your_next".tr,
                                          style: theme.textTheme.bodyMedium))
                                ])),
                        CustomImageView(
                            imagePath: ImageConstant.imgEllipse6,
                            height: 62.adaptSize,
                            width: 62.adaptSize,
                            radius: BorderRadius.circular(31.h),
                            margin: EdgeInsets.only(left: 43.h))
                      ]))),
          SizedBox(height: 15.v),
          _buildGunungkidulYogyakarta(context),
          SizedBox(height: 24.v)
        ]));
  }

  /// Section Widget
  Widget _buildTen(BuildContext context) {
    return Align(
        alignment: Alignment.topCenter,
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 30.h, vertical: 56.v),
            decoration: AppDecoration.gradientSecondaryContainerToGray,
            child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(height: 228.v),
                  Text("msg_near_your_location".tr,
                      style: CustomTextStyles.titleLarge23)
                ])));
  }

  /// Section Widget
  Widget _buildFortyNine(BuildContext context) {
    return BlocBuilder<Screen1EnglishBloc, Screen1EnglishState>(
        builder: (context, state) {
      return CarouselSlider.builder(
          options: CarouselOptions(
              height: 314.v,
              initialPage: 0,
              autoPlay: true,
              viewportFraction: 1.0,
              enableInfiniteScroll: false,
              scrollDirection: Axis.horizontal,
              onPageChanged: (index, reason) {
                state.sliderIndex = index;
              }),
          itemCount:
              state.screen1EnglishModelObj?.fortynineItemList.length ?? 0,
          itemBuilder: (context, index, realIndex) {
            FortynineItemModel model =
                state.screen1EnglishModelObj?.fortynineItemList[index] ??
                    FortynineItemModel();
            return FortynineItemWidget(model, onTapViewHierarchy: () {
              onTapViewHierarchy(context);
            });
          });
    });
  }

  /// Section Widget
  Widget _buildSeeAll(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(top: 3.v, bottom: 5.v),
        child: OutlineGradientButton(
            padding:
                EdgeInsets.only(left: 1.h, top: 1.v, right: 1.h, bottom: 1.v),
            strokeWidth: 1.h,
            gradient: LinearGradient(
                begin: Alignment(0.08, 0.5),
                end: Alignment(1.14, 0),
                colors: [appTheme.deepPurple300, appTheme.cyan20003]),
            corners: Corners(
                topLeft: Radius.circular(11),
                topRight: Radius.circular(11),
                bottomLeft: Radius.circular(11),
                bottomRight: Radius.circular(11)),
            child: CustomOutlinedButton(
                height: 23.v,
                width: 53.h,
                text: "lbl_see_all".tr,
                buttonStyle: CustomButtonStyles.outlineTL11,
                buttonTextStyle: CustomTextStyles.labelMediumIndigoA100)));
  }

  /// Section Widget
  Widget _buildThirtyFive(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 30.h),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("msg_find_trip_partners".tr,
                  style: CustomTextStyles.titleLarge23),
              _buildSeeAll(context)
            ]));
  }

  /// Section Widget
  Widget _buildUserProfile(BuildContext context) {
    return Align(
        alignment: Alignment.centerRight,
        child: SizedBox(
            height: 175.v,
            child: BlocSelector<Screen1EnglishBloc, Screen1EnglishState,
                    Screen1EnglishModel?>(
                selector: (state) => state.screen1EnglishModelObj,
                builder: (context, screen1EnglishModelObj) {
                  return ListView.separated(
                      padding: EdgeInsets.only(left: 24.h),
                      scrollDirection: Axis.horizontal,
                      separatorBuilder: (context, index) {
                        return SizedBox(width: 18.h);
                      },
                      itemCount:
                          screen1EnglishModelObj?.userprofileItemList.length ??
                              0,
                      itemBuilder: (context, index) {
                        UserprofileItemModel model = screen1EnglishModelObj
                                ?.userprofileItemList[index] ??
                            UserprofileItemModel();
                        return UserprofileItemWidget(model);
                      });
                })));
  }

  /// Section Widget
  Widget _buildBottomBar(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 20.h),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.vertical(top: Radius.circular(15.h)),
            gradient: LinearGradient(
                begin: Alignment(0.5, 0),
                end: Alignment(0.5, 2.54),
                colors: [appTheme.blueA100, appTheme.cyan20004])),
        child: CustomImageView(
            imagePath: ImageConstant.imgGroup12,
            height: 27.v,
            width: 255.h,
            margin: EdgeInsets.fromLTRB(47.h, 22.v, 48.h, 19.v)));
  }

  /// Navigates to the screen2EnglishScreen when the action is triggered.
  onTapViewHierarchy(BuildContext context) {
    NavigatorService.pushNamed(
      AppRoutes.screen2EnglishScreen,
    );
  }
}
