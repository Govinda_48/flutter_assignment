import 'package:flutter/material.dart';
import 'package:sagar_s_application2/core/app_export.dart';
import '../models/userprofile_item_model.dart';

// ignore: must_be_immutable
class UserprofileItemWidget extends StatelessWidget {
  UserprofileItemWidget(
    this.userprofileItemModelObj, {
    Key? key,
  }) : super(
          key: key,
        );

  UserprofileItemModel userprofileItemModelObj;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 106.h,
      child: Align(
        alignment: Alignment.centerRight,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 11.h,
                vertical: 10.v,
              ),
              decoration: AppDecoration.gradientDeepPurpleToCyan.copyWith(
                borderRadius: BorderRadiusStyle.roundedBorder25,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CustomImageView(
                    imagePath: userprofileItemModelObj?.userImage,
                    height: 82.adaptSize,
                    width: 82.adaptSize,
                    radius: BorderRadius.circular(
                      41.h,
                    ),
                  ),
                  SizedBox(height: 6.v),
                  SizedBox(
                    width: 45.h,
                    child: Text(
                      userprofileItemModelObj.userName!,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.center,
                      style: theme.textTheme.labelMedium,
                    ),
                  ),
                  SizedBox(height: 7.v),
                ],
              ),
            ),
            SizedBox(height: 6.v),
            Text(
              userprofileItemModelObj.distance!,
              style: theme.textTheme.titleMedium,
            ),
          ],
        ),
      ),
    );
  }
}
