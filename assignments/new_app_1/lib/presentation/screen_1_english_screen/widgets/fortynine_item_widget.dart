import 'package:flutter/material.dart';
import 'package:sagar_s_application2/core/app_export.dart';
import 'package:sagar_s_application2/widgets/custom_elevated_button.dart';
import '../models/fortynine_item_model.dart';

// ignore: must_be_immutable
class FortynineItemWidget extends StatelessWidget {
  FortynineItemWidget(
    this.fortynineItemModelObj, {
    Key? key,
    this.onTapViewHierarchy,
  }) : super(
          key: key,
        );

  FortynineItemModel fortynineItemModelObj;

  VoidCallback? onTapViewHierarchy;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomLeft,
      child: GestureDetector(
        onTap: () {
          onTapViewHierarchy!.call();
        },
        child: Container(
          margin: EdgeInsets.only(left: 214.h),
          padding: EdgeInsets.symmetric(
            horizontal: 24.h,
            vertical: 17.v,
          ),
          decoration: AppDecoration.gradientDeepPurpleAToCyan.copyWith(
            borderRadius: BorderRadiusStyle.roundedBorder40,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 5.v),
              Container(
                width: 161.h,
                margin: EdgeInsets.only(right: 55.h),
                child: Text(
                  fortynineItemModelObj.text!,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: CustomTextStyles.labelLargeOnPrimaryContainerBold_1
                      .copyWith(
                    height: 1.41,
                  ),
                ),
              ),
              SizedBox(height: 10.v),
              SizedBox(
                height: 229.v,
                width: 216.h,
                child: Stack(
                  alignment: Alignment.bottomRight,
                  children: [
                    CustomImageView(
                      imagePath: fortynineItemModelObj?.image,
                      height: 216.adaptSize,
                      width: 216.adaptSize,
                      radius: BorderRadius.circular(
                        21.h,
                      ),
                      alignment: Alignment.topCenter,
                    ),
                    _buildViewHierarchy(context),
                    _buildFiftyOne(context),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Section Widget
  Widget _buildViewHierarchy(BuildContext context) {
    return CustomElevatedButton(
      width: 54.h,
      text: "lbl_8_7".tr,
      buttonStyle: CustomButtonStyles.none,
      decoration: CustomButtonStyles.gradientGreenAToBlueDecoration,
      alignment: Alignment.bottomRight,
    );
  }

  /// Section Widget
  Widget _buildFiftyOne(BuildContext context) {
    return CustomElevatedButton(
      width: 54.h,
      text: "lbl_1_3km".tr,
      buttonStyle: CustomButtonStyles.none,
      decoration: CustomButtonStyles.gradientGreenAToBlueDecoration,
      alignment: Alignment.bottomLeft,
    );
  }
}
