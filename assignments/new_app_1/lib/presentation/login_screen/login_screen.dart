import 'package:flutter/material.dart';
import 'package:outline_gradient_button/outline_gradient_button.dart';
import 'package:sagar_s_application2/core/app_export.dart';
import 'package:sagar_s_application2/widgets/custom_elevated_button.dart';
import 'package:sagar_s_application2/widgets/custom_outlined_button.dart';
import 'bloc/login_bloc.dart';
import 'models/login_model.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key})
      : super(
          key: key,
        );

  static Widget builder(BuildContext context) {
    return BlocProvider<LoginBloc>(
      create: (context) => LoginBloc(LoginState(
        loginModelObj: LoginModel(),
      ))
        ..add(LoginInitialEvent()),
      child: LoginScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      builder: (context, state) {
        return SafeArea(
          child: Scaffold(
            body: SizedBox(
              width: double.maxFinite,
              child: Column(
                children: [
                  Spacer(),
                  SizedBox(
                    height: 127.v,
                    width: 256.h,
                    child: Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                        Align(
                          alignment: Alignment.topCenter,
                          child: RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: "lbl_libur".tr,
                                  style: CustomTextStyles.novaOvalff84efc2,
                                ),
                                TextSpan(
                                  text: "lbl_by".tr,
                                  style: CustomTextStyles.novaOvalff649bec,
                                ),
                              ],
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Text(
                            "msg_your_trip_and_tell".tr,
                            style: theme.textTheme.headlineSmall,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 53.v),
                  SizedBox(
                    height: 489.v,
                    width: double.maxFinite,
                    child: Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        CustomImageView(
                          imagePath: ImageConstant.imgVector2,
                          height: 477.v,
                          width: 390.h,
                          alignment: Alignment.center,
                        ),
                        CustomImageView(
                          imagePath: ImageConstant.imgVector3,
                          height: 453.v,
                          width: 390.h,
                          alignment: Alignment.topCenter,
                        ),
                        _buildExcitedLetSGetSection(context),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  /// Section Widget
  Widget _buildExcitedLetSGetSection(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 75.h,
          vertical: 111.v,
        ),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(
              ImageConstant.imgGroup3,
            ),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            SizedBox(height: 64.v),
            Text(
              "msg_excited_let_s_get".tr,
              style: theme.textTheme.labelLarge,
            ),
            SizedBox(height: 7.v),
            CustomElevatedButton(
              height: 49.v,
              text: "lbl_login".tr,
              buttonStyle: CustomButtonStyles.none,
              decoration: CustomButtonStyles.gradientPrimaryToIndigoADecoration,
              buttonTextStyle: CustomTextStyles.titleMediumOnPrimaryContainer,
            ),
            SizedBox(height: 13.v),
            OutlineGradientButton(
              padding: EdgeInsets.only(
                left: 3.h,
                top: 3.v,
                right: 3.h,
                bottom: 3.v,
              ),
              strokeWidth: 3.h,
              gradient: LinearGradient(
                begin: Alignment(0.02, 1.11),
                end: Alignment(0.97, 0.11),
                colors: [
                  appTheme.blue300,
                  appTheme.tealA100,
                ],
              ),
              corners: Corners(
                topLeft: Radius.circular(24),
                topRight: Radius.circular(24),
                bottomLeft: Radius.circular(24),
                bottomRight: Radius.circular(24),
              ),
              child: CustomOutlinedButton(
                text: "msg_create_an_account".tr,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
