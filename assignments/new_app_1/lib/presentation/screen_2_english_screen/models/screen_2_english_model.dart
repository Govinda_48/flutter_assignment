// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';
import 'twentysix_item_model.dart';

/// This class defines the variables used in the [screen_2_english_screen],
/// and is typically used to hold data that is passed between different parts of the application.
class Screen2EnglishModel extends Equatable {
  Screen2EnglishModel({this.twentysixItemList = const []}) {}

  List<TwentysixItemModel> twentysixItemList;

  Screen2EnglishModel copyWith({List<TwentysixItemModel>? twentysixItemList}) {
    return Screen2EnglishModel(
      twentysixItemList: twentysixItemList ?? this.twentysixItemList,
    );
  }

  @override
  List<Object?> get props => [twentysixItemList];
}
