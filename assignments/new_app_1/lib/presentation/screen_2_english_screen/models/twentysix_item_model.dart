// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';

/// This class is used in the [twentysix_item_widget] screen.
class TwentysixItemModel extends Equatable {
  TwentysixItemModel({
    this.km,
    this.isSelected,
  }) {
    km = km ?? "1.3km";
    isSelected = isSelected ?? false;
  }

  String? km;

  bool? isSelected;

  TwentysixItemModel copyWith({
    String? km,
    bool? isSelected,
  }) {
    return TwentysixItemModel(
      km: km ?? this.km,
      isSelected: isSelected ?? this.isSelected,
    );
  }

  @override
  List<Object?> get props => [km, isSelected];
}
