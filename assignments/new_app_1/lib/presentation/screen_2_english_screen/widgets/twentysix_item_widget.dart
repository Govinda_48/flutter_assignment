import 'package:flutter/material.dart';
import 'package:sagar_s_application2/core/app_export.dart';
import '../models/twentysix_item_model.dart';

// ignore: must_be_immutable
class TwentysixItemWidget extends StatelessWidget {
  TwentysixItemWidget(
    this.twentysixItemModelObj, {
    Key? key,
    this.onSelectedChipView,
  }) : super(
          key: key,
        );

  TwentysixItemModel twentysixItemModelObj;

  Function(bool)? onSelectedChipView;

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        canvasColor: Colors.transparent,
      ),
      child: RawChip(
        padding: EdgeInsets.symmetric(
          horizontal: 17.h,
          vertical: 9.v,
        ),
        showCheckmark: false,
        labelPadding: EdgeInsets.zero,
        label: Text(
          twentysixItemModelObj.km!,
          style: TextStyle(
            color: theme.colorScheme.onPrimaryContainer,
            fontSize: 17.11111068725586.fSize,
            fontFamily: 'Nunito',
            fontWeight: FontWeight.w700,
          ),
        ),
        selected: (twentysixItemModelObj.isSelected ?? false),
        backgroundColor: Colors.transparent,
        selectedColor: appTheme.blue20001.withOpacity(0.2),
        shape: (twentysixItemModelObj.isSelected ?? false)
            ? RoundedRectangleBorder(
                side: BorderSide.none,
                borderRadius: BorderRadius.circular(
                  20.h,
                ),
              )
            : RoundedRectangleBorder(
                side: BorderSide.none,
                borderRadius: BorderRadius.circular(
                  20.h,
                ),
              ),
        onSelected: (value) {
          onSelectedChipView?.call(value);
        },
      ),
    );
  }
}
