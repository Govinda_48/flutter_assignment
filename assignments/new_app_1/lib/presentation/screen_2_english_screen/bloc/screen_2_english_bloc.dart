import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import '/core/app_export.dart';
import '../models/twentysix_item_model.dart';
import 'package:sagar_s_application2/presentation/screen_2_english_screen/models/screen_2_english_model.dart';
part 'screen_2_english_event.dart';
part 'screen_2_english_state.dart';

/// A bloc that manages the state of a Screen2English according to the event that is dispatched to it.
class Screen2EnglishBloc
    extends Bloc<Screen2EnglishEvent, Screen2EnglishState> {
  Screen2EnglishBloc(Screen2EnglishState initialState) : super(initialState) {
    on<Screen2EnglishInitialEvent>(_onInitialize);
    on<UpdateChipViewEvent>(_updateChipView);
  }

  _onInitialize(
    Screen2EnglishInitialEvent event,
    Emitter<Screen2EnglishState> emit,
  ) async {
    emit(state.copyWith(
        screen2EnglishModelObj: state.screen2EnglishModelObj
            ?.copyWith(twentysixItemList: fillTwentysixItemList())));
    Future.delayed(const Duration(milliseconds: 3000), () {
      NavigatorService.popAndPushNamed(
        AppRoutes.loginScreen,
      );
    });
  }

  _updateChipView(
    UpdateChipViewEvent event,
    Emitter<Screen2EnglishState> emit,
  ) {
    List<TwentysixItemModel> newList = List<TwentysixItemModel>.from(
        state.screen2EnglishModelObj!.twentysixItemList);
    newList[event.index] =
        newList[event.index].copyWith(isSelected: event.isSelected);
    emit(state.copyWith(
        screen2EnglishModelObj: state.screen2EnglishModelObj
            ?.copyWith(twentysixItemList: newList)));
  }

  List<TwentysixItemModel> fillTwentysixItemList() {
    return List.generate(2, (index) => TwentysixItemModel());
  }
}
