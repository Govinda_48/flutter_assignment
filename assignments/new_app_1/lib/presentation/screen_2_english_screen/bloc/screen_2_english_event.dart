// ignore_for_file: must_be_immutable

part of 'screen_2_english_bloc.dart';

/// Abstract class for all events that can be dispatched from the
///Screen2English widget.
///
/// Events must be immutable and implement the [Equatable] interface.
@immutable
abstract class Screen2EnglishEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

/// Event that is dispatched when the Screen2English widget is first created.
class Screen2EnglishInitialEvent extends Screen2EnglishEvent {
  @override
  List<Object?> get props => [];
}

///Event for changing ChipView selection
class UpdateChipViewEvent extends Screen2EnglishEvent {
  UpdateChipViewEvent({
    required this.index,
    this.isSelected,
  });

  int index;

  bool? isSelected;

  @override
  List<Object?> get props => [
        index,
        isSelected,
      ];
}
