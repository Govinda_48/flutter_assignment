// ignore_for_file: must_be_immutable

part of 'screen_2_english_bloc.dart';

/// Represents the state of Screen2English in the application.
class Screen2EnglishState extends Equatable {
  Screen2EnglishState({this.screen2EnglishModelObj});

  Screen2EnglishModel? screen2EnglishModelObj;

  @override
  List<Object?> get props => [
        screen2EnglishModelObj,
      ];

  Screen2EnglishState copyWith({Screen2EnglishModel? screen2EnglishModelObj}) {
    return Screen2EnglishState(
      screen2EnglishModelObj:
          screen2EnglishModelObj ?? this.screen2EnglishModelObj,
    );
  }
}
