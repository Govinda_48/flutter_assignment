import 'package:flutter/material.dart';
import 'package:sagar_s_application2/core/app_export.dart';
import 'package:sagar_s_application2/widgets/app_bar/appbar_title_image.dart';
import 'package:sagar_s_application2/widgets/app_bar/custom_app_bar.dart';
import 'package:sagar_s_application2/widgets/custom_elevated_button.dart';
import 'package:sagar_s_application2/widgets/custom_icon_button.dart';
import '../screen_2_english_screen/widgets/twentysix_item_widget.dart';
import 'bloc/screen_2_english_bloc.dart';
import 'models/screen_2_english_model.dart';
import 'models/twentysix_item_model.dart';

class Screen2EnglishScreen extends StatelessWidget {
  const Screen2EnglishScreen({Key? key}) : super(key: key);

  static Widget builder(BuildContext context) {
    return BlocProvider<Screen2EnglishBloc>(
        create: (context) => Screen2EnglishBloc(
            Screen2EnglishState(screen2EnglishModelObj: Screen2EnglishModel()))
          ..add(Screen2EnglishInitialEvent()),
        child: Screen2EnglishScreen());
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: _buildAppBar(context),
            body: SizedBox(
                width: SizeUtils.width,
                child: SingleChildScrollView(
                    child: Column(children: [
                  SizedBox(
                      height: 566.v,
                      width: double.maxFinite,
                      child: Stack(alignment: Alignment.center, children: [
                        Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                                height: 196.v,
                                width: double.maxFinite,
                                margin: EdgeInsets.only(top: 81.v),
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                        begin: Alignment(0.5, 0),
                                        end: Alignment(0.5, 1),
                                        colors: [
                                      theme.colorScheme.secondaryContainer,
                                      appTheme.gray40000
                                    ])))),
                        Align(
                            alignment: Alignment.center,
                            child: Container(
                                padding: EdgeInsets.all(43.h),
                                decoration: AppDecoration
                                    .gradientDeepPurpleAToCyan
                                    .copyWith(
                                        borderRadius:
                                            BorderRadiusStyle.roundedBorder40),
                                child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(height: 105.v),
                                      Container(
                                          width: 242.h,
                                          margin: EdgeInsets.only(right: 61.h),
                                          child: Text(
                                              "msg_air_terjun_luweng".tr,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: CustomTextStyles
                                                  .titleMediumOnPrimaryContainer18
                                                  .copyWith(height: 1.41))),
                                      SizedBox(height: 7.v),
                                      CustomImageView(
                                          imagePath:
                                              ImageConstant.imgAirTerjunLuwe,
                                          height: 303.adaptSize,
                                          width: 303.adaptSize,
                                          radius: BorderRadius.circular(21.h))
                                    ]))),
                        _buildTwentyThree(context),
                        _buildTwentySix(context)
                      ])),
                  SizedBox(height: 20.v),
                  CustomElevatedButton(
                      height: 47.v,
                      width: 235.h,
                      text: "lbl_get_direction".tr,
                      leftIcon: Container(
                          margin: EdgeInsets.only(right: 9.h),
                          child: CustomImageView(
                              imagePath: ImageConstant.imgNavigation,
                              height: 31.adaptSize,
                              width: 31.adaptSize)),
                      buttonStyle: CustomButtonStyles.none,
                      decoration:
                          CustomButtonStyles.gradientGreenAToCyanDecoration,
                      buttonTextStyle:
                          CustomTextStyles.titleMediumOnPrimaryContainer17_1),
                  SizedBox(height: 12.v),
                  Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                          padding: EdgeInsets.only(left: 51.h),
                          child: Text("msg_traveller_s_review".tr,
                              style: CustomTextStyles.titleMediumBlack900))),
                  SizedBox(height: 7.v),
                  _buildTwenty(context),
                  SizedBox(height: 34.v),
                  _buildNineteen(context)
                ]))),
            bottomNavigationBar: _buildBottomBar(context)));
  }

  /// Section Widget
  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return CustomAppBar(
        centerTitle: true,
        title: AppbarTitleImage(imagePath: ImageConstant.imgRectangle18));
  }

  /// Section Widget
  Widget _buildTwentyThree(BuildContext context) {
    return Align(
        alignment: Alignment.topCenter,
        child: Card(
            clipBehavior: Clip.antiAlias,
            elevation: 0,
            margin: EdgeInsets.all(0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadiusStyle.customBorderBL40),
            child: Container(
                height: 118.v,
                width: double.maxFinite,
                padding: EdgeInsets.symmetric(horizontal: 26.h, vertical: 19.v),
                decoration: BoxDecoration(
                    borderRadius: BorderRadiusStyle.customBorderBL40),
                child: Stack(alignment: Alignment.bottomCenter, children: [
                  Align(
                      alignment: Alignment.bottomCenter,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CustomIconButton(
                                height: 45.adaptSize,
                                width: 45.adaptSize,
                                child: CustomImageView()),
                            Padding(
                                padding: EdgeInsets.only(left: 243.h),
                                child: CustomIconButton(
                                    height: 45.adaptSize,
                                    width: 45.adaptSize,
                                    child: CustomImageView()))
                          ])),
                  Align(
                      alignment: Alignment.bottomCenter,
                      child: Padding(
                          padding: EdgeInsets.only(bottom: 3.v),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                CustomImageView(
                                    imagePath: ImageConstant.imgMegaphone,
                                    height: 16.v,
                                    width: 24.h,
                                    margin:
                                        EdgeInsets.symmetric(vertical: 9.v)),
                                Padding(
                                    padding:
                                        EdgeInsets.only(left: 97.h, top: 3.v),
                                    child: RichText(
                                        text: TextSpan(children: [
                                          TextSpan(
                                              text: "lbl_libur".tr,
                                              style: CustomTextStyles
                                                  .titleLargeNovaOvalff84efc2),
                                          TextSpan(
                                              text: "lbl_by".tr,
                                              style: CustomTextStyles
                                                  .titleLargeNovaOvalff649bec)
                                        ]),
                                        textAlign: TextAlign.left)),
                                CustomImageView(
                                    imagePath: ImageConstant.imgSearch,
                                    height: 31.adaptSize,
                                    width: 31.adaptSize,
                                    margin: EdgeInsets.only(
                                        left: 98.h, bottom: 3.v))
                              ])))
                ]))));
  }

  /// Section Widget
  Widget _buildTwentySix(BuildContext context) {
    return Align(
        alignment: Alignment.bottomLeft,
        child: Padding(
            padding: EdgeInsets.only(left: 44.h),
            child: BlocSelector<Screen2EnglishBloc, Screen2EnglishState,
                    Screen2EnglishModel?>(
                selector: (state) => state.screen2EnglishModelObj,
                builder: (context, screen2EnglishModelObj) {
                  return Wrap(
                      runSpacing: 18.67.v,
                      spacing: 18.67.h,
                      children: List<Widget>.generate(
                          screen2EnglishModelObj?.twentysixItemList.length ?? 0,
                          (index) {
                        TwentysixItemModel model =
                            screen2EnglishModelObj?.twentysixItemList[index] ??
                                TwentysixItemModel();
                        return TwentysixItemWidget(model,
                            onSelectedChipView: (value) {
                          context.read<Screen2EnglishBloc>().add(
                              UpdateChipViewEvent(
                                  index: index, isSelected: value));
                        });
                      }));
                })));
  }

  /// Section Widget
  Widget _buildTwenty(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 28.h),
        padding: EdgeInsets.symmetric(horizontal: 14.h, vertical: 9.v),
        decoration: AppDecoration.gradientDeepPurpleToIndigoA
            .copyWith(borderRadius: BorderRadiusStyle.roundedBorder25),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              CustomImageView(
                  imagePath: ImageConstant.imgEllipse10,
                  height: 43.adaptSize,
                  width: 43.adaptSize,
                  radius: BorderRadius.circular(21.h),
                  margin: EdgeInsets.only(left: 3.h, top: 14.v, bottom: 19.v)),
              Expanded(
                  child: Padding(
                      padding: EdgeInsets.only(left: 18.h, bottom: 3.v),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("lbl_lowry_white".tr,
                                style: CustomTextStyles
                                    .labelLargeOnPrimaryContainerBold),
                            SizedBox(
                                width: 239.h,
                                child: Text("msg_very_clean_place".tr,
                                    maxLines: 6,
                                    overflow: TextOverflow.ellipsis,
                                    style: theme.textTheme.bodySmall!
                                        .copyWith(height: 1.07)))
                          ])))
            ]));
  }

  /// Section Widget
  Widget _buildNineteen(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 20.h),
        padding: EdgeInsets.symmetric(horizontal: 12.h, vertical: 6.v),
        decoration: AppDecoration.gradientDeepPurpleToIndigoA10001
            .copyWith(borderRadius: BorderRadiusStyle.roundedBorder25),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              CustomImageView(
                  imagePath: ImageConstant.imgEllipse11,
                  height: 46.adaptSize,
                  width: 46.adaptSize,
                  radius: BorderRadius.circular(23.h),
                  margin: EdgeInsets.only(left: 5.h, bottom: 1.v)),
              Container(
                  height: 32.v,
                  width: 254.h,
                  margin: EdgeInsets.only(left: 19.h, top: 2.v, bottom: 12.v),
                  child: Stack(alignment: Alignment.bottomCenter, children: [
                    Align(
                        alignment: Alignment.topLeft,
                        child: Text("lbl_ethan_johnson".tr,
                            style: theme.textTheme.titleSmall)),
                    Align(
                        alignment: Alignment.bottomCenter,
                        child: Text("msg_i_really_like_the".tr,
                            style: CustomTextStyles.bodySmall9))
                  ]))
            ]));
  }

  /// Section Widget
  Widget _buildBottomBar(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 20.h),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.vertical(top: Radius.circular(15.h)),
            gradient: LinearGradient(
                begin: Alignment(0.5, 0),
                end: Alignment(0.5, 2.54),
                colors: [appTheme.blueA100, appTheme.cyan20004])),
        child: CustomImageView(
            imagePath: ImageConstant.imgGroup12,
            height: 27.v,
            width: 255.h,
            margin: EdgeInsets.fromLTRB(47.h, 22.v, 48.h, 19.v)));
  }
}
