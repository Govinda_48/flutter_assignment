class ImageConstant {
  // Image folder path
  static String imagePath = 'assets/images';

  // Screen 2 English images
  static String imgRectangle18 = '$imagePath/img_rectangle_18.png';

  static String imgNavigation = '$imagePath/img_navigation.png';

  static String imgEllipse10 = '$imagePath/img_ellipse_10.png';

  static String imgEllipse11 = '$imagePath/img_ellipse_11.png';

  // Login Screen images
  static String imgVector2 = '$imagePath/img_vector_2.png';

  static String imgVector3 = '$imagePath/img_vector_3.png';

  static String imgGroup3 = '$imagePath/img_group_3.png';

  // Screen 1 English images
  static String imgEllipse6 = '$imagePath/img_ellipse_6.png';

  static String imgLocation = '$imagePath/img_location.png';

  static String imgPantaiWatuKodok = '$imagePath/img_pantai_watu_kodok.png';

  static String imgPantaiWatuKodok158x144 =
      '$imagePath/img_pantai_watu_kodok_158x144.png';

  static String imgArrowLeft = '$imagePath/img_arrow_left.svg';

  static String imgArrowLeftCyan20002 =
      '$imagePath/img_arrow_left_cyan_200_02.svg';

  static String imgArrowRight = '$imagePath/img_arrow_right.svg';

  static String imgEllipse9 = '$imagePath/img_ellipse_9.png';

  static String imgEllipse12 = '$imagePath/img_ellipse_12.png';

  static String imgEllipse1182x82 = '$imagePath/img_ellipse_11_82x82.png';

  static String imgEllipse111 = '$imagePath/img_ellipse_11_1.png';

  // Common images
  static String imgAirTerjunLuwe = '$imagePath/img_air_terjun_luwe.png';

  static String imgMegaphone = '$imagePath/img_megaphone.svg';

  static String imgSearch = '$imagePath/img_search.png';

  static String imgGroup12 = '$imagePath/img_group_12.png';

  static String imageNotFound = 'assets/images/image_not_found.png';
}
