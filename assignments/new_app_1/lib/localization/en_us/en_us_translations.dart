final Map<String, String> enUs = {
  // Screen 2 English Screen
  "lbl_ethan_johnson": "Ethan Johnson",
  "lbl_get_direction": "Get Direction",
  "lbl_lowry_white": "Lowry White ",
  "msg_i_really_like_the":
      "I really like the view of the river! Also this brings me the vibe of African",
  "msg_traveller_s_review": "Traveller’s Review",
  "msg_very_clean_place":
      "Very clean place and really a great place to rest your mind! I would love to come back here again!\n\nOne tip: The track is not eligible for sedans and some cars, so recommended to bring a bike or walk instead of using cars and busses",

  // Login Screen Screen
  "lbl_login": "Login",
  "msg_create_an_account": "Create an Account",
  "msg_excited_let_s_get": "Excited? Let’s get into it!",
  "msg_your_trip_and_tell": "Your trip-and-tell friend",

  // Screen 1 English Screen
  "lbl_1_7km": "1.7km",
  "lbl_1_8km": "1.8km",
  "lbl_2_7km": "2.7km",
  "lbl_6_7km": "6.7km",
  "lbl_9_2": "9.2",
  "lbl_angela_lahm": "Angela Lahm",
  "lbl_hey": "Hey ",
  "lbl_hey_mchardyan": "Hey McHardyan!",
  "lbl_mchardyan": "McHardyan!",
  "lbl_see_all": "See All",
  "lbl_trevor_mchall": "Trevor McHall",
  "msg_find_trip_partners": "Find Trip Partners!",
  "msg_gunungkidul_yogyakarta": "Gunungkidul, Yogyakarta",
  "msg_michelle_sauniere": "Michelle Sauniere",
  "msg_near_your_location": "Near Your Location",
  "msg_pantai_ngrumput": "Pantai Ngrumput, Gunungkidul",
  "msg_phillips_eduardo": "Phillips Eduardo",
  "msg_watu_kodok_gunungkidul": "Watu Kodok, Gunungkidul",
  "msg_where_your_next": "Where your next trip going to be?",

  // Common String
  "lbl_1_3km": "1.3km",
  "lbl_8_7": "8.7",
  "lbl_by": "By",
  "lbl_libur": "Libur",
  "lbl_liburby": "LiburBy",
  "msg_air_terjun_luweng": "Air Terjun Luweng Sampang, Gunungkidul",

// Network Error String
  "msg_network_err": "Network Error",
  "msg_something_went_wrong": "Something Went Wrong!",
};
